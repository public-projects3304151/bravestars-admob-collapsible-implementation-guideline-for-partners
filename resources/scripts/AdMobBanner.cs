using AppsFlyerSDK;
using GoogleMobileAds.Api;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using BravestarsSDK;

public enum BANNER_POSITION : int {
    TOP = 0,
    BOTTOM = 1
}

public enum BANNER_TYPE : int {
    NORMAL = 0,
    COLLAPSIBLE = 1
}

public class AdMobBanner : BS_Singleton<AdMobBanner> {
    [SerializeField] private string ANDROID_AD_UNIT_ID;
    [SerializeField] private string IOS_AD_UNIT_ID;
    [SerializeField] private bool IsDebug = false;
    [SerializeField] private BANNER_POSITION _bannerPosition;

    private string _adUnitId;
    private BannerView _bannerView;
    private Coroutine _countDownReloadAd = null;
    private float _countdownTime  = float.MaxValue;

    private void Awake() {
        DontDestroyOnLoad(gameObject);
    }

    private void CreateBannerView() {
        if (_bannerView != null)
            DestroyBannerView();

        if (IsDebug) {
            _adUnitId = "ca-app-pub-3940256099942544/2014213617";
        } else {
#if UNITY_ANDROID
            _adUnitId = ANDROID_AD_UNIT_ID;
#elif UNITY_IOS
            _adUnitId = IOS_AD_UNIT_ID;
#endif
        }
        AdSize adaptiveSize = AdSize.GetCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(AdSize.FullWidth);
        AdPosition bannerPosition = _bannerPosition == BANNER_POSITION.TOP ? AdPosition.Top : AdPosition.Bottom;
        _bannerView = new BannerView(_adUnitId, adaptiveSize, bannerPosition);
        ListenToAdEvents();
    }

    public void LoadAd(BANNER_TYPE type, bool isAutoReloadAds = false) {
        if (!isAutoReloadAds && _countdownTime < BS_Data.GetIntData("collaps_interval_time"))
            return;

        if (_bannerView == null)
            CreateBannerView();

        var adRequest = new AdRequest();

        if (type == BANNER_TYPE.COLLAPSIBLE) {
            string bannerPos = _bannerPosition == BANNER_POSITION.TOP ? "top" : "bottom";
            adRequest.Extras.Add("collapsible", bannerPos);
        } else if (type == BANNER_TYPE.NORMAL) {
            adRequest.Extras.Add("collapsible_request_id", Guid.NewGuid().ToString());
        }

        if (_countDownReloadAd != null)
            StopCoroutine(_countDownReloadAd);

        _countdownTime = 0f;

        _countDownReloadAd = StartCoroutine(CountDownReloadAd(BS_Data.GetIntData("collapsible_reload_time")));
        _bannerView.LoadAd(adRequest);
    }

    private IEnumerator CountDownReloadAd(int time) {
        yield return new WaitForSeconds(time);
        LoadAd(BANNER_TYPE.COLLAPSIBLE, true);
    }

    private void Update()
    {
        _countdownTime += Time.deltaTime;
    }


    private void ListenToAdEvents() {
        // Raised when an ad is loaded into the banner view.
        _bannerView.OnBannerAdLoaded += () => {
            Debug.Log("Collapsible banner view loaded an ad with response : " + _bannerView.GetResponseInfo());
        };
        // Raised when an ad fails to load into the banner view.
        _bannerView.OnBannerAdLoadFailed += (LoadAdError error) => {
            Debug.LogError("Collapsible banner view failed to load an ad with error : " + error);
        };
        // Raised when the ad is estimated to have earned money.
        _bannerView.OnAdPaid += (AdValue adValue) => {
            if (adValue == null)
                return;
            try {
                LogAdImpression(adValue);
                //Debug.Log(String.Format($"Banner view paid {0} {1}.", adValue.Value, adValue.CurrencyCode));
            } catch (Exception) {

            }
        };
        // Raised when an impression is recorded for an ad.
        _bannerView.OnAdImpressionRecorded += () => {
            Debug.Log("Collapsible banner view recorded an impression.");
        };
        // Raised when a click is recorded for an ad.
        _bannerView.OnAdClicked += () => {
            Debug.Log("Collapsible banner view was clicked.");
        };
        // Raised when an ad opened full screen content.
        _bannerView.OnAdFullScreenContentOpened += () => {
            Debug.Log("Collapsible banner view full screen content opened.");
        };
        // Raised when the ad closed full screen content.
        _bannerView.OnAdFullScreenContentClosed += () => {
            Debug.Log("Collapsible banner view full screen content closed.");
        };
    }

    public void DestroyBannerView() {
        if (_bannerView != null) {
            _bannerView.Destroy();
            _bannerView = null;
        }
    }

    private void LogAdImpression(AdValue adValue) {
        try {
            double value = (double)adValue.Value / 1000000f;
            ResponseInfo responseInfo = _bannerView.GetResponseInfo();
            AdapterResponseInfo loadedAdapterResponseInfo = responseInfo.GetLoadedAdapterResponseInfo();
            string adSourceId = loadedAdapterResponseInfo.AdSourceId;
            string adSourceName = loadedAdapterResponseInfo.AdSourceName;
            //BS_Utils.Log($"Collapsible: Impression Data Value {value.GetType()} {value}");
            Firebase.Analytics.Parameter[] AdParameters = {
                new Firebase.Analytics.Parameter("ad_platform", "Admob"),
                new Firebase.Analytics.Parameter("ad_source", adSourceName),
                new Firebase.Analytics.Parameter("ad_unit_name", adSourceId),
                new Firebase.Analytics.Parameter("ad_format", "Collapsible"),
                new Firebase.Analytics.Parameter("value", value),
                new Firebase.Analytics.Parameter("currency", adValue.CurrencyCode),
                    };
            if (FirebaseManager.IsReady) {
                BS_Utils.Log("Collapsible: Firebase ready! Send Collapsible impression");
                Firebase.Analytics.FirebaseAnalytics.LogEvent("ad_impression", AdParameters);
            } else {
                StartCoroutine(SendFirebaseImpression(AdParameters));
            }

            var dic = new Dictionary<string, string>();
            dic.Add("ad_platform", "Admob");
            dic.Add("ad_source", adSourceName);
            dic.Add("ad_unit_name", adSourceId);
            dic.Add("ad_format", "Collapsible");
            dic.Add("value", value.ToString());
            dic.Add("currency", adValue.CurrencyCode);
            AppsFlyerAdRevenue.logAdRevenue(adSourceName, AppsFlyerAdRevenueMediationNetworkType.AppsFlyerAdRevenueMediationNetworkTypeGoogleAdMob, value, adValue.CurrencyCode, dic);
            //BS_Utils.Log($"Log collapsible adRevenue - adSourceName {adSourceName} - value {value} - currencyCode {adValue.CurrencyCode}");
        } catch {
            Debug.LogWarning("Log ad collapsible impression fail!");
        }
    }

    private IEnumerator SendFirebaseImpression(Firebase.Analytics.Parameter[] AdParameters, float delay = 2f) {
        //BS_Utils.Log($"Collapsible: Firebase Not ready! resend collapsible banner impression after {delay} seconds");
        yield return new WaitForSeconds(delay);
        if (FirebaseManager.IsReady) {
            BS_Utils.Log("Collapsible: Firebase ready! Send collapsible impression");
            Firebase.Analytics.FirebaseAnalytics.LogEvent("ad_impression", AdParameters);
        } else {
            StartCoroutine(SendFirebaseImpression(AdParameters, delay * 2));
        }
    }
}
