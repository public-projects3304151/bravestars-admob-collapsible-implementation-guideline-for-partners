
# Hướng dẫn cách triển khai Google AdMob Collapsible banner ad


## Thông tin
- Hướng dẫn sử dụng AdMobBanner script để show ra collapsible banner của google mobile ads.
- Chức năng bao gồm: 
   - Show ra collapsible ads.
   - Show ads test.
   - Tự động reload lại một ad sau mỗi X giây. 
   - Log Firebase và Appsflyer event revenue của ads.


## Điều kiện
- Đã tạo parameter `collapsible_reload_time` trên Firebase Remote Config với định dạng number và có giá trị mặc định là 30 (Số giây reload lại ads).
- Đã tạo parameter `collapsible_interval_time` trên Firebase Remote Config với định dạng number và có giá trị mặc định là 1 (Số giây tối thiểu cần chờ từ lần show collapsible ad gần nhất cần phải chờ trước khi show collapsible ad lần tiếp theo).
- Đã gắn AOA, hoặc khởi tạo Google Mobile Ads trước đó. 

## Triển khai

### Bước 1 - Tạo AdmobBanner object
Tạo một object trống AdmobBanner sau đó add component [AdmobBanner.cs](resources/scripts/AdMobBanner.cs) ở scene có index = 0 của game (Scene đầu tiên chạy khi init game). 

![image](resources/images/1_create_admob_banner_object.png)

Có 3 thông số cần lưu ý bao gồm: 
- `Ad unit id`: là collapsible id ads của project.
- `IsDebug`: Là trạng thái test, khi đó không cần gắn Ad unit id mà sẽ dùng một id ads test để chạy thử.
- `Banner Position`: Vị trí show ads, hiện tại chỉ có 2 vị trí top và bottom. Recommend bottom vì top hiện đang show ra 2 banner khi ở trạng thái collapsible.

### Bước 2 - Tạo biến Firebase Remote Config
- Thêm biến remote config `collapsible_reload_time` để reload lại ad.
- Thêm biến remote config `collapsible_interval_time` để set khoảng thời gian tối thiểu cần chờ giữa 2 lần show collapsible ad liên tiếp.

### Bước 3 - Call method show ads
Call method `AdMobBanner.Instance.LoadAd` theo kịch bản riêng của mỗi game. 
```csharp
AdMobBanner.Instance.LoadAd(BANNER_TYPE.COLLAPSIBLE);
```
Trong đó tham số truyền vào là 
- `BannerType` - có 2 loại là 
   - `Collapsible`
   - `Normal` - là trạng thái banner thu nhỏ lại.
- `IsAutoLoadAds` - type `bool`, trong đó:
   - `true`: tự động reload lại collapsible ad sau số giây được cấu hình trên biến remote `collapsible_reload_time`
   - `false`: show collapsible ad thủ công
## Chú ý
* Trong quá trình tích hợp và test show collapsible ads chỉ cần quan tâm show ads đúng với kịch bản ở môi trường debug (tức là dùng id ads test). Sau khi thay ID thật và ở môi trường release nếu không lên collapsible ads thì là phía Google AdMob không trả về.

## Release Notes
### Version 1.0 - 15/05/2024

### Version 1.1 - 20/06/2024
* Reset biến đếm thời gian `_countdownTime = 0` sau mỗi lần show collapsible ads
* Chỉ show collapsible ads khi biến đếm thời gian `_countdownTime` >= giá trị config trong biến remote

## Hỗ trợ
Trong trường hợp có bất cứ vấn đề gì cần hỗ trợ vui lòng liên hệ với team Business Development của Bravestars để nhận hỗ trợ.